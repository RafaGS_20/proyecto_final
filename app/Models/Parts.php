<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sets;
use App\Models\parts_sets;

class Parts extends Model
{
    use HasFactory;

    protected $fillable = [

        'id',

        'name', 

        'ref', 

        'position',

        'stock'

    ];

    /**
     * 
     * Método creado para entablar una relación N:M entre la pieza y el conjunto.
     * 
     */
    public function sets()
    {
        return $this->belongsToMany(parts_sets::class, 'parts_id');
    }

}
