<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    use HasFactory;

    protected $fillable = [

        'id',

        'name', 

        'description', 

    ];

    /**
     * 
     * Método creado para vincular usuarios a los talleres, relación 1:N.
     * 
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * 
     * Método creado para vincular sets a los talleres, relación 1:N.
     * 
     */
    public function sets()
    {
        return $this->hasMany(Sets::class, 'workshops_id');
    }
}