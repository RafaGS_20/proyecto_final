<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class parts_sets extends Model
{
    use HasFactory;


    protected $fillable = [

        'id',

        'sets_id',

        'workshops_id'

    ];

    /**
     * 
     * 
     * 
     */
    public function workshop()
    {
        return $this->belongsTo(Sets::class, 'sets_id');
    }

    public function parts()
    {
        return $this->belongsToMany(parts_sets::class, 'parts_id');
    }
    
}