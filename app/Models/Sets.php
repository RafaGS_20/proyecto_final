<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sets extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [

        'id',

        'name', 

        'ref', 

        'workshops_id'

    ];
    /**
     * 
     * Método creado para vincular un taller al usuario, relación 1:N.
     * 
     */
    public function workshop()
    {
        return $this->belongsTo(workshop::class, 'workshops_id');
    }

    /**
     * 
     * Método creado para entablar una relación N:M entre la pieza y el conjunto.
     * 
     */
    public function parts()
    {
        return $this->belongsToMany(parts_sets::class, 'sets_id');
    }
    
}
