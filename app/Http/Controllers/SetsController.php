<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sets;
use App\Models\Workshop;
use App\Models\Parts;
use App\Models\parts_sets;
use Illuminate\Support\Facades\DB;

class SetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sets = Sets::all();
        
        return view('sets.index', ['sets' => $sets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $workshops = Workshop::all();

        return view ('sets.create', ['workshops' => $workshops]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workshops = Workshop::all();

        $data = $request->all();

        Sets::create($data);

        $request->session()->flash('correcto','Se ha creado el registro.'); 

        return redirect('conjuntos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sets = Sets::findOrFail($id);

        // Obtengo todos los sets asociados a la pieza. 
        $all_parts = parts_sets::where('sets_id', $id)->get();
        // Obtengo los valores del set asociado a la pieza. 
        $compatibles = Parts::where('id', $all_parts[0]['parts_id'])->first();
        
        return view('sets.show', ['sets' => $sets], [ 'compatibles' => $compatibles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $sets = Sets::findOrFail($id);
        return view('sets.edit' , ['sets' => $sets]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array_slice( $request->all(), 2);

        Sets::whereId($id)->update($data);

        $request->session()->flash('correcto','Se ha editado el registro.'); 

        return redirect ('conjuntos/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sets::where('id', $id)->delete();

        $request->session()->flash('correcto','Se ha borrado el registro.'); 

        return redirect('conjuntos/');
    }
}
