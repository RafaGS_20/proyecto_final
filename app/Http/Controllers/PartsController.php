<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parts;
use App\Models\Sets;
use App\Models\parts_sets;
use Illuminate\Support\Facades\DB;

class PartsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Sistema de búsqueda.
        $texto = trim($request->get('texto')); 
        $parts = DB::table('parts')
        ->select()
        ->where('name', 'LIKE', '%'.$texto.'%')->paginate(10);

        return view('parts.index', compact('parts','texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('parts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        Parts::create($data);

        $request->session()->flash('correcto','Se ha creado el registro.'); 

        return redirect('piezas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parts = Parts::findOrFail($id);

        // Obtengo todos los sets asociados a la pieza. 
        $all_sets = parts_sets::where('parts_id', $id)->get();
        // Obtengo los valores del set asociado a la pieza. 
        $compatibles = Sets::where('id', $all_sets[0]['sets_id'])->first();
        
        return view('parts.show', ['parts' => $parts], [ 'compatibles' => $compatibles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parts = Parts::findOrFail($id);
        return view('parts.edit' , ['parts' => $parts]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array_slice( $request->all(), 2);

        Parts::whereId($id)->update($data);

        $request->session()->flash('correcto','Se ha editado el registro.'); 

        return redirect ('piezas/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Parts::where('id', $id)->delete();

        $request->session()->flash('correcto','Se ha borrado el registro.'); 

        return redirect('piezas/');
    }
}
