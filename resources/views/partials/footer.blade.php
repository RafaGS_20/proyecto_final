<style>
    footer{
        background-color: #78866b;
        margin-top: 10px;
    }

    .darken-grey-text {
        color: #2E2E2E;
    }
</style>

<!--Footer-->
<footer class="page-footer deep-purple center-on-small-only pt-0">

    <!--Footer Links-->
    <div class="container">

        <!--Grid row-->
        <div class="row pt-5 mb-3 text-center d-flex justify-content-center">

            <!--Grid column-->
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a href="#!"> Inicio </a></h6>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a href="#!"> Buzón de sugerencias </a></h6>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a href="#!"> Preguntas frecuentes </a></h6>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a href="#!"> Contacto </a></h6>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a href="#!"> Sobre nosotros </a></h6>
            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->

        <hr class="rgba-white-light" style="margin: 0 15%;">

        <!--Grid row-->
        <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

            <!--Grid column-->
            <div class="col-md-8 col-12 mt-5">
                <p style="line-height: 1.7rem">
                Proyecto desarrollado haciendo uso del framework Laravel. Esta aplicación 
                no está totalmente desarrollada, es solo una práctica, para más información 
                contacte a través de correo electrónico: rafael_gs_lp@hotmail.com
                </p>

            </div>
            <!--Grid column-->

        </div>

    </div>
    <!--/Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright" >
        <div class="container-fluid">
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img
                    alt="Licencia Creative Commons" style="border-width:0"
                    src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>
            <br />Esta obra está bajo una
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Licencia Creative Commons
                Atribución-NoComercial-SinDerivadas 4.0 Internacional</a>.
            <p> Todos los derechos reservados.</p>
        </div>
    </div>
    <!--/Copyright-->

</footer>
<!--/Footer-->