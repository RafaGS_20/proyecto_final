@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
                Nuevo conjunto
            </div>
            <div class="card-body" style="padding:30px">

                <form method="POST" enctype="multipart/form-data" action="{{ url('conjuntos') }}"
                    style="display:inline">
                    {{-- TODO: Protección contra CSRF --}}
                    @csrf

                    <div class="form-group">
                        <label for="name">Nombre </label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        <label for="ref"> Referencia </label>
                        <input type="text" name="ref" id="ref" class="form-control" value="{{ old('ref') }}">
                    </div>

                    <div class="form-group">
                        <label for="workshops_id"> Taller responsable </label>
                        <select name="workshops_id" id="workshops_id" class="form-control"
                            value="{{ old('workshop_id') }}">

                        @foreach($workshops as $key => $workshop)
                            
                            <option value=" {{$workshop->id}} " name = "workshop_id"> {{$workshop->name}} </option>

                        @endforeach

                        </select>

                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Añadir conjunto
                        </button>
                    </div>
                </form>
                {{-- TODO: Cerrar formulario --}}

            </div>
        </div>
    </div>
</div>

<br>

@stop