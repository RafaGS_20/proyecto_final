@extends('layouts.master')

@section('content')

@if(Session::has('correcto'))

<div class="alert alert-success"> {{ Session::get('correcto') }}</div>

@endif

<div class="row">

    @foreach( $sets as $key => $set )

    <div class="card col-2" style="margin: 5px">

        <h5 style="min-height:45px;margin:5px 0 10px 0"> {{ $set->name }}

            <i class="icon-info-sign"> <a href="{{ '/conjuntos'.'/'. $set->id }}"> + </a> </i>

        </h5>

        <p> Referencia: {{ $set->ref }} </p>

        <p> Taller responsable: {{ $set->workshop->name}} </p>


    </div>

    @endforeach

</div>

@stop