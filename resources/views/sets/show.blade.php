@extends('layouts.master')

@section('content')

@if(Session::has('correcto'))

<div class="alert alert-success"> {{ Session::get('correcto') }}</div>

@endif

<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <h4> {{ $sets->name }} </h4>

        <p> Referencia: {{ $sets->ref }} </p>

        <p> Taller responsable: {{ $sets->workshop->name }}
            <button class="btn-outline-primary">
                <a href="{{ '/talleres'.'/'. $sets->workshop->id }}"> + </a>
            </button> </p>

        <a href="{{ '/conjuntos'}}" class="btn btn-dark ml-2"> Volver </a>

        <a href="{{ '/conjuntos' .'/' . $sets['id'] . '/edit'}}" class="btn btn-primary ml-2"> Editar </a>

        <form method="POST" action=" {{  url( '/conjuntos/' .$sets->id ) }}" style="display:inline">

            @method('DELETE')

            @csrf

            @include('sets.destroy')

            <a href="" data-target="#modal-delete-{{$sets->id}}" data-toggle="modal"
                class="btn btn-danger ml-2">Eliminar</a>

        </form>

    </div>

</div>

<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <h4> Piezas compatibles con el conjunto: </h4>

    </div>
       
    <div class="col-12 text-center" style="margin: 5px;">

        <p> {{ $compatibles->name }}
            <button class="btn-outline-primary">
                <a href="{{ '/piezas'.'/'. $compatibles->id }}"> + </a>
            </button> </p>

    </div>
    
</div>

@stop