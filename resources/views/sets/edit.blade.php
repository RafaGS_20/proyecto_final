@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
                Modificar conjunto
            </div>
            <div class="card-body" style="padding:30px">
                <form method="POST" enctype="multipart/form-data" action="{{url('conjuntos/'.$sets->id)}}"
                    style="display:inline">
                    {{-- TODO: Abrir el formulario e indicar el método POST --}}
                    @method('PUT')
                    {{-- TODO: Protección contra CSRF --}}
                    @csrf

                    <div class="form-group">
                        <label for="name"> Nombre </label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $sets->name }}">
                    </div>

                    <div class="form-group">
                        <label for="ref"> Referencia </label>
                        <input type="text" name="ref" id="ref" class="form-control" value="{{ $sets->ref }}">
                    </div>

                    <div class="form-group">
                        <label for="workshop_id"> Taller asociado </label>
                        <input type="text" name="workshop_id" id="workshop_id" class="form-control" value="{{ $sets->workshops_id }}">
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Modificar conjunto
                        </button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

@stop