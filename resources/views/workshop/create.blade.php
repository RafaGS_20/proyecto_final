@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
                Nuevo taller
            </div>
            <div class="card-body" style="padding:30px">

                <form method="POST" enctype="multipart/form-data" action="{{ url('talleres') }}"
                    style="display:inline">
                    {{-- TODO: Protección contra CSRF --}}
                    @csrf

                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        {{-- TODO: Completa el input para la imagen --}}
                        <label for="description"> Descripción </label>
                        <textarea name="description" id="description" class="form-control">
                        {{ old('description') }} </textarea>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Añadir taller
                        </button>
                    </div>
                </form>
                {{-- TODO: Cerrar formulario --}}

            </div>
        </div>
    </div>
</div>

<br>

@stop