@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
                Modificar taller
            </div>
            <div class="card-body" style="padding:30px">
                <form method="POST" enctype="multipart/form-data" action="{{url('talleres/'.$workshops->id)}}"
                    style="display:inline">
                    {{-- TODO: Abrir el formulario e indicar el método POST --}}
                    @method('PUT')
                    {{-- TODO: Protección contra CSRF --}}
                    @csrf

                    <div class="form-group">
                        <label for="name"> Nombre </label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $workshops->name }}">
                    </div>

                    <div class="form-group">
                        <label for="description"> Descripción </label>
                        <textarea name="description" id="description" class="form-control">
                        {{ $workshops->description }} </textarea>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Modificar taller
                        </button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

@stop