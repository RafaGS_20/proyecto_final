@extends('layouts.master')

@section('content')

@if(Session::has('correcto'))

<div class="alert alert-success"> {{ Session::get('correcto') }}</div>

@endif

@foreach( $workshops as $key => $workshop )

<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <h4 style="min-height:45px;margin:5px 0 10px 0">

            {{ $workshop->name }}

            <button class="btn-outline-primary">
            <a href="{{ '/talleres'.'/'. $workshop->id }}"> + </a></button>

        </h4>

    </div>

</div>

@endforeach

@stop