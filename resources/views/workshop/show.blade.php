@extends('layouts.master')

@section('content')

@if(Session::has('correcto'))

<div class="alert alert-success"> {{ Session::get('correcto') }}</div>

@endif

<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <h4> {{ $workshops['name'] }} </h4>

    </div>

</div>

<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <p> {{ $workshops['description'] }} </p>

        <a href="{{ '/talleres'}}" class="btn btn-dark ml-2"> Volver </a>

        <a href="{{ '/talleres' .'/' . $workshops['id'] . '/edit'}}" class="btn btn-primary ml-2"> Editar </a>

        <form method="POST" action=" {{  url( '/talleres/' .$workshops->id ) }}" style="display:inline">

            @method('DELETE')

            @csrf

            @include('workshop.destroy')

            <a href="" data-target="#modal-delete-{{$workshops->id}}" data-toggle="modal"
                class="btn btn-danger ml-2">Eliminar</a>

        </form>

    </div>

</div>

<br>

<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <h4> Conjuntos asociados al taller: </h4>

    </div>

</div>

@for ($i = 0; $i < sizeof($workshops->sets) ; $i++)
    <div class="col-12 text-center" style="margin: 5px;">
        <p> Nombre del conjunto: {{ $workshops->sets[$i]->name }}
            <button class="btn-outline-primary">
                <a href="{{ '/conjuntos'.'/'. $workshops->sets[$i]->id }}"> + </a>
            </button>
        </p>

    </div>
    @endfor

    @stop