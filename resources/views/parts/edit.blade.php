@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
                Modificar pieza
            </div>
            <div class="card-body" style="padding:30px">
                <form method="POST" enctype="multipart/form-data" action="{{url('piezas/'.$parts->id)}}"
                    style="display:inline">
                    {{-- TODO: Abrir el formulario e indicar el método POST --}}
                    @method('PUT')
                    {{-- TODO: Protección contra CSRF --}}
                    @csrf

                    <div class="form-group">
                        <label for="name"> Nombre </label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $parts->name }}">
                    </div>

                    <div class="form-group">
                        <label for="ref"> Referencia </label>
                        <input type="text" name="ref" id="ref" class="form-control" value="{{ $parts->ref }}">
                    </div>

                    <div class="form-group">
                        <label for="position"> Ubicación </label>
                        <input type="text" name="position" id="position" class="form-control" value="{{ $parts->position }}">
                    </div>

                    
                    <div class="form-group">
                        <label for="stock"> Stock </label>
                        <input type="text" name="stock" id="stock" class="form-control" value="{{ $parts->stock }}">
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Modificar pieza
                        </button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

@stop