@extends('layouts.master')

@section('content')

<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
                Nueva pieza
            </div>
            <div class="card-body" style="padding:30px">

                <form method="POST" enctype="multipart/form-data" action="{{ url('piezas') }}"
                    style="display:inline">
                    {{-- TODO: Protección contra CSRF --}}
                    @csrf

                    <div class="form-group">
                        <label for="name">Nombre </label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        <label for="ref"> Referencia </label>
                        <input type="text" name="ref" id="ref" class="form-control" value="{{ old('ref') }}">
                    </div>

                    <div class="form-group">
                        <label for="position"> Posición </label>
                        <input type="text" name="position" id="position" class="form-control" value="{{ old('position') }}">
                    </div>

                    <div class="form-group">
                        <label for="stock"> Stock </label>
                        <input type="text" name="stock" id="stock" class="form-control" value="{{ old('stock') }}">
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Añadir pieza
                        </button>
                    </div>
                </form>
                {{-- TODO: Cerrar formulario --}}

            </div>
        </div>
    </div>
</div>

<br>

@stop