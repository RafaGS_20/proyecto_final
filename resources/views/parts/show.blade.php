@extends('layouts.master')

@section('content')

@if(Session::has('correcto'))

<div class="alert alert-success"> {{ Session::get('correcto') }}</div>

@endif

<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <h4> {{ $parts->name }} </h4>

    </div>

</div>

<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <p> Referencia: {{ $parts->ref }} </p>

        <p> Ubicación: {{ $parts->position }} </p>

        <p> Stock: {{ $parts->stock }} </p>

        <a href="{{ '/piezas'}}" class="btn btn-dark ml-2"> Volver </a>

        <a href="{{ '/piezas' .'/' . $parts['id'] . '/edit'}}" class="btn btn-primary ml-2"> Editar </a>

        <form method="POST"
            action=" {{  url( '/piezas/' .$parts->id ) }}"
            style="display:inline">

            @method('DELETE')

            @csrf

            @include('parts.destroy')

            <a href="" data-target="#modal-delete-{{$parts->id}}" data-toggle="modal" class="btn btn-danger ml-2">Eliminar</a>

        </form>

    </div>

</div>

<br>


<div class="row">

    <div class="col-12 text-center" style="margin: 5px;">

        <h4> Conjuntos compatibles con la pieza: </h4>

    </div>
       
    <div class="col-12 text-center" style="margin: 5px;">

        <p> {{ $compatibles->name }}
            <button class="btn-outline-primary">
                <a href="{{ '/conjuntos'.'/'. $compatibles->id }}"> + </a>
            </button> </p>

    </div>
    
</div>


@stop