@extends('layouts.master')

@section('content')

@if(Session::has('correcto'))

<div class="alert alert-success"> {{ Session::get('correcto') }}</div>

@endif

<div class="row">

    @foreach( $parts as $key => $part )

    <div class="card col-2" style="margin: 5px">

        <h5 style="min-height:45px;margin:5px 0 10px 0"> {{ $part->name }}

            <i class="icon-info-sign"> <a href="{{ '/piezas'.'/'. $part->id }}"> + </a> </i>

        </h5>

        <p> Referencia: {{ $part->ref }} </p>

        <p> Ubicación: {{ $part->position }} </p>

        <p> Stock: {{ $part->stock }} </p>

    </div>

    @endforeach

</div>

@stop