<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sets;

class SetsTableSeeder extends Seeder
{
    // Array con los talles de nuestra app. 
    private $arrayWorkshops = array (
        array(

            'name' => 'Radio V3',
        
            'ref' => '47968521463', 
        
            'workshops_id' => '3', 
        
        ), 
        
        array(
        
            'name' => 'Radio PR-4G',
        
            'ref' => '7832154962589', 
        
            'workshops_id' => '3', 
        ), 
        
        array(
        
            'name' => 'Gafas VN',
        
            'ref' => '9832451596321', 
        
            'workshops_id' => '2', 
        ), 
        
        array(
        
            'name' => 'Monóculo VN',
        
            'ref' => '4536741582349', 
        
            'workshops_id' => '2', 
        ), 
        
        array(
        
            'name' => 'Peugeout 206',
        
            'ref' => '40001485967896', 
        
            'workshops_id' => '1', 
        ), 
        
        array(
        
            'name' => 'Anibal Santana',
        
            'ref' => '47832124567896', 
        
            'workshops_id' => '1', 
        )
        
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Inserción de datos a través de array. 
       foreach ($this->arrayWorkshops as $workshop) {

        $w = new Sets;

        $w->name = $workshop['name']; 

        $w->ref = $workshop['ref'];

        $w->workshops_id = $workshop['workshops_id'];

        $w->save();
        
       }
    }
}