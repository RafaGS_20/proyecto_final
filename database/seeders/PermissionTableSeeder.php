<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
  
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'parts-list',
           'parts-create',
           'parts-edit',
           'parts-delete',
           'sets-list',
           'sets-create',
           'sets-edit',
           'sets-delete',
           'workshops-list',
           'workshops-create',
           'workshops-edit',
           'workshops-delete'
        ];
     
        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}
