<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Parts;

class PartsTableSeeder extends Seeder
{
    // Array con los talles de nuestra app. 
    private $arrayWorkshops = array (
        array(

            'name' => 'Motor de arranque',
        
            'ref' => '4000124567896', 
        
            'position' => '9200', 
        
            'stock' => '1'
        ), 
        
        array(
        
            'name' => 'Bujías',
        
            'ref' => '2000235698741', 
        
            'position' => '9150', 
        
            'stock' => '3'
        ), 
        
        array(
        
            'name' => 'Pastillas de freno',
        
            'ref' => '9000235896321', 
        
            'position' => '9600', 
        
            'stock' => '4'
        ), 
        
        array(
        
            'name' => 'Rueda',
        
            'ref' => '8000254136985', 
        
            'position' => '9578', 
        
            'stock' => '6'
        ), 
        
        array(
        
            'name' => 'Emisor infrarojos',
        
            'ref' => '4578962315846', 
        
            'position' => '9600', 
        
            'stock' => '7'
        ), 
        
        array(
        
            'name' => 'Receptor infrarojos',
        
            'ref' => '2030623012589', 
        
            'position' => '9100', 
        
            'stock' => '8'
        ), 
        
        array(
        
            'name' => 'Base de antena',
        
            'ref' => '4578321657896', 
        
            'position' => '9500', 
        
            'stock' => '2'
        ), 
        
        array(
        
            'name' => 'Radio teléfono',
        
            'ref' => '78562148625896', 
        
            'position' => '9800', 
        
            'stock' => '1'
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Inserción de datos a través de array. 
       foreach ($this->arrayWorkshops as $workshop) {

        $w = new Parts;

        $w->name = $workshop['name']; 

        $w->ref = $workshop['ref'];

        $w->position = $workshop['position'];

        $w->stock = $workshop['stock'];

        $w->save();
        
       }
    }
}
