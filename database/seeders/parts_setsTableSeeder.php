<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\parts_sets;

class parts_setsTableSeeder extends Seeder
{
    // Array con los talles de nuestra app. 
    private $arrayWorkshops = array (
        array(
            'sets_id' => '1',

            'parts_id' => '7'
        ), 
        array(
            'sets_id' => '1',

            'parts_id' => '8'
        ), 
        array(
            'sets_id' => '2',

            'parts_id' => '8'
        ), 
        array(
            'sets_id' => '2',

            'parts_id' => '7'
        ), 
        array(
            'sets_id' => '3',

            'parts_id' => '5'
        ), 
        array(
            'sets_id' => '4',

            'parts_id' => '6'
        ), 
        array(
            'sets_id' => '5',

            'parts_id' => '2'
        ), 
        array(
            'sets_id' => '5',

            'parts_id' => '3'
        ), 
        array(
            'sets_id' => '6',

            'parts_id' => '1'
        ), 
        array(
            'sets_id' => '6',

            'parts_id' => '4'
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Inserción de datos a través de array. 
       foreach ($this->arrayWorkshops as $workshop) {

        $w = new parts_sets;

        $w->sets_id = $workshop['sets_id']; 

        $w->parts_id = $workshop['parts_id'];

        $w->save();
        
       }
    }
}