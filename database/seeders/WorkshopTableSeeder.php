<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Workshop;

class WorkshopTableSeeder extends Seeder
{
    // Array con los talles de nuestra app. 
    private $arrayWorkshops = array (
        array(
            'name' => 'Mecánica',

            'description' => 'El taller de mecánica se dedica a la reparación y mantenimiento de
            toda clase de vehículos. Además, gracias a nuestro personal especializado
            podemos efectuar reparaciones de todo tipo, desde chapa, a electrónica
            a problemas de tipo mecánico.'
        ), 
        array (
            'name' => 'Electrónica',

            'description' => 'En el taller de electrónica se realizan relaciones y comprobaciones de
            todo tipo a cualquier componente electrónico de los diversos sistemas.
            Dentro de este amplio espectro se incluyen los sistemas eléctricos de
            todos los vehículos, así como instalaciones eléctricas de las instalaciones
            y toda la electrónica de los componentes presentes en las mismas.'
        ),
        array (
            'name' => 'Transmisiones',

            'description' => 'En el taller de transmisiones se realizan reparaciones de todos los sistemas
            de comunicación usados para el día a día de nuestros clientes y nuestros
            productos de las instalaciones y talleres.'
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Inserción de datos a través de array. 
       foreach ($this->arrayWorkshops as $workshop) {

        $w = new Workshop;

        $w->name = $workshop['name']; 

        $w->description = $workshop['description'];

        $w->save();
        
       }
    }
}
