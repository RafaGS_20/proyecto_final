<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartsSetsTable extends Migration
{
    public $timestamps = false;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts_sets', function (Blueprint $table) {
            
            $table->bigInteger('sets_id')->unsigned();
            $table->bigInteger('parts_id')->unsigned();

            $table->foreign('sets_id')->references('id')->on('sets');
            $table->foreign('parts_id')->references('id')->on('parts');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts_sets');
    }
}
