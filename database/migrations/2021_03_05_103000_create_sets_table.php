<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sets', function (Blueprint $table) {

            $table->id();
            // Nombre comercial del set. 
            $table->string('name');
            // Número de referencia, es único y está compuesto por 13 dígitos o letras. 
            $table->string('ref', 99)->unique();
            
            // Id del taller como clave foránea. 
            $table->unsignedBigInteger('workshops_id');
            $table->foreign('workshops_id')->references('id')->on('workshops');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sets');
    }
}