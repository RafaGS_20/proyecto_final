<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts', function (Blueprint $table) {

            $table->id();
            // Nombre comercial de la pieza. 
            $table->string('name');
            // Número de referencia, es único y está compuesto por 13 dígitos o letras. 
            $table->string('ref', 13)->unique();
            // Posición en la que se encuentra dentro del almacén. 
            $table->string('position', 4);
            // Cantidad del artículo que tenemos en el almacén. 
            $table->string('stock', 4); 

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts');
    }
}