<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WorkshopController;
use App\Http\Controllers\SetsController;
use App\Http\Controllers\PartsController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', function () {
    return view('welcome');
});

Route::get('auth/login', function () {
    return view('login');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/inicio', function () {
    return view('home');
});

Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function() {
    Route::resource('talleres', WorkshopController::class);
    Route::resource('piezas', PartsController::class);
    Route::resource('conjuntos', SetsController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
});